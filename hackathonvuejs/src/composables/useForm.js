import { ref } from 'vue'
import Schema from 'async-validator';

export function useForm(options) {

    const formErrors = ref({});

    const form = ref(options.form);

    const validatorValues = ref(options.validatorValues);

    const validator = new Schema(validatorValues.value);

    return {
        form,
        formErrors,
        validatorValues,
        validator
    }
}