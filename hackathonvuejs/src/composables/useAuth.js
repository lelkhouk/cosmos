
import {getAuth} from '../service/api.service';
import {useRouter} from 'vue-router';
import { ref } from 'vue';


const user = ref(null);

export function useAuth(){
    const identifier = ref('');
    const password = ref('');
    const errors = ref({});
    

    const router = useRouter()
    const validateInput = ()=>{
        console.log("Identifier:", identifier.value, "Password:", password.value);
    
        //si on ne renseigne pas les champs
        if(!identifier.value){
            errors.value.identifier = 'username requis'
        }else{
            errors.value.identifier = null
        }
        if(!password.value){
            errors.value.password = 'password requis'
        }else{
            errors.value.password = null
        }
    }
    const responseData = ref([]);
    async function login(form) {
        try {
        const response = await getAuth(form);
            console.log(form);
            console.log("Réponse de l'API après la connexion:", response);
            responseData.value.push(response);
            console.log('data '+responseData);
            

            localStorage.setItem('AUTH_TOKEN', response.jwt);
            localStorage.setItem('user', JSON.stringify(response.user));
            user.value = response.user;
            router.push({ name: 'home' });

            } catch(error) {
                console.error("Erreur lors de la connexion:", error);
                err.response.data.error.details.errors.forEach((error) => errors.value.push(error.message))
            }
    }

    function logout(){
        localStorage.clear();
        router.push({ name: 'login' });
    }

    return {
        login,
        identifier,
        password,
        errors,
        validateInput,
        logout,
        user
    }
}