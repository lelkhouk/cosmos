// useCat.js

import { ref } from 'vue';
import { getCategorie } from '../service/api.service';

export function useCat() {
    const categories = ref([]);
    const categoriePromise = getCategorie();

    const fetchCategorie = async () => {
        try {
            const response = await categoriePromise;
            categories.value = response;
        } catch (error) {
            console.error('Erreur lors de la récupération des catégories :', error);
        }
    }

    const getCategoryName = (categorieId) => {
        const categorie = categories.value.find(cat => cat.id == categorieId);
        return categorie ? categorie.nom : 'Catégorie inconnue';
    }

    return {
        fetchCategorie,
        categories,
        getCategoryName
    }
}
