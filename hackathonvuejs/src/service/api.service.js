import instance from "@/config/axios";

//EXEMPLE


export async function getProduit() {
    try {
        const response = await instance.get('/produit');
        return response.data;
    } catch (ex) {
        console.log(ex)
    }
}

export async function getCategorie() {
    try {
        const response = await instance.get('/categorie');
        return response.data;
    } catch (ex) {
        console.log(ex)
    }
}

//LOGIN
export async function getAuth(form){
    try{
        const response= await instance.post('/auth/local',form);
        console.log(response);
        return response.data;
    }catch(ex){
        console.log(ex)
    }
}


export function isAuthenticated() {
    return !!localStorage.getItem('AUTH_TOKEN');
}