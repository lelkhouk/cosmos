import { createRouter, createWebHashHistory } from 'vue-router'
import Home from "./components/pages/Home.vue"
import Login from "./components/pages/Login.vue"

export const router = createRouter({
    history: createWebHashHistory(),
    routes: [
        {
            path: '/login',
            name: 'home',
            component: Home
        },
        {
            path: '/',
            name: 'login',
            component: Login
        },
        {
            path: '/logout',
            name: 'logout'
        }
    ]
});